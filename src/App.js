import './App.css';
import { React, useState, useEffect } from 'react';
import {Container} from 'react-bootstrap';

import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Product from './pages/Product'
import ProductView from './pages/ProductView'
import Admin from './pages/Admin'
import Deactivate from './pages/Deactivate'
import Update from './pages/Update'
import Login from './pages/Login'
import Logout from './pages/Logout'
import Register from './pages/Register'
import Create from './pages/Create'
import ErrorPage from './pages/ErrorPage'
import Footer from './components/Footer'
import { UserProvider } from './UserContext';

export default function App() {

  const [user, setUser] = useState({
      id: null,
      isAdmin: null
  })

  const unsetUser = () =>{
    localStorage.clear()
  }

  useEffect( () => {

    fetch("http://cryptic-savannah-49957.herokuapp.com/api/users/details", {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    })
    .then(res => res.json())
    .then(data => {

      if(typeof data._id !== "undefined") {
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      } else {
        setUser({
          id: null,
          isAdmin: null
        })
      }

    })

  }, [])

  return (

    <UserProvider value = {{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar />
        <Container>

          <Routes style={{
        backgroundColor: 'blue',
        width: '100px',
        height: '100px'
      }}>

            <Route path="/" element={<Home />} />
            <Route path="/product" element={<Product />} />
            <Route path="/product/:productId" element={<ProductView/>} />
            <Route path="/login" element={<Login />} />
            <Route path="/register" element={<Register />} />
            <Route path="/create" element={<Create />} />
            <Route path="/all" element={<Admin />} />
            <Route path="/archive/:productId" element={<Deactivate />} />
            <Route path="/update/:productId" element={<Update />} />
            <Route path="/logout" element={<Logout />} />
            <Route path="*" element={<ErrorPage />} />
          </Routes>
        </Container>  
      </Router>
      <Footer /> 
    </UserProvider>

  );
}


