import { useState, useEffect, useContext } from 'react';
import {Container, Form, Row, Col, Card, Button} from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function ProductView() {

  const { user } = useContext(UserContext);

  const navigate = useNavigate();

  const { productId } = useParams()

  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(0);




  const update = (e) => {
  	e.preventDefault();


    fetch(`http://cryptic-savannah-49957.herokuapp.com/caps/product/${productId}/update`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`
      },
      body: JSON.stringify({
        productId: productId,
        name: name,
        description: description,
        price: price
      })

    }) 
    .then(res => res.json())
    .then(data => {
      

      if (data === true) {
        Swal.fire({
          title: "Successfully Updated",
          icon: "success",
          text: "You have successfully update this item."
        })

        navigate("/all")

      } else {
        Swal.fire({
          title: "Something went wrong",
          icon: "error",
          text: "Please try again"
        })
      }
    })
  }

  useEffect(()=> {
    fetch(`http://cryptic-savannah-49957.herokuapp.com/caps/product/${productId}`)
    .then(res => res.json())
    .then(data => {
      

      setName(data.name)
      setDescription(data.description)
      setPrice(data.price)
    })
  }, [productId])

  return(

		<Form onSubmit= {(e) => update(e)}>
		<h1 className="text-center">Update</h1>

			<Form.Group 
		  		className="mb-3" 
		  		controlId="firstName">
		    <Form.Label>{name}</Form.Label>
		    <Form.Control 
		    	type="text" 
		    	placeholder="Enter First Name"
		    	value={name}
		    	onChange={e => {
		    		setName(e.target.value)
		    		
		    	}} 
		    	required
		    />

		  </Form.Group>

		  <Form.Group 
		  		className="mb-3" 
		  		controlId="firstName">
		    <Form.Label>{description}</Form.Label>
		    <Form.Control 
		    	type="text" 
		    	placeholder="Enter First Name"
		    	value={description}
		    	onChange={e => {
		    		setDescription(e.target.value)
		    		
		    	}} 
		    	required
		    />

		  </Form.Group>
		 <Form.Group 
		  		className="mb-3" 
		  		controlId="firstName">
		    <Form.Label>{price}</Form.Label>
		    <Form.Control 
		    	type="text" 
		    	placeholder="Enter First Name"
		    	value={price}
		    	onChange={e => {
		    		setPrice(e.target.value)
		    		
		    	}} 
		    	required
		    />

		  </Form.Group>


		  	<Button variant="primary" type="submit" id="submitBtn">
		  	  Submit
		  	</Button>



		</Form>


		)
}