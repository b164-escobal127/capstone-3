import { Fragment, useContext } from 'react'
import {Navbar, Nav, NavDropdown, Container} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import { Form, FormControl, Button} from 'react-bootstrap';
import UserContext from '../UserContext';


export default function AppNavbar() {

const { user } = useContext(UserContext);

return(
<Navbar as={Link} to="/" bg="warning" expand="lg">
  <Container fluid>

    <Navbar.Brand ><img
          src="../images.png"
          width="30"
          height="30"
          className="d-inline-block align-top"
        />

        Kjames' Shop </Navbar.Brand>

    <Navbar.Toggle aria-controls="navbarScroll" />
    {
              user.id !== null && user.isAdmin === true ?
              <Fragment>
              <Nav.Link as={Link} to="/all" exact >All Products</Nav.Link>
              </Fragment>
              :

     <Fragment>
    <Navbar.Collapse id="navbarScroll">

   
      <Nav
        className="me-auto my-2 my-lg-0"
        style={{ maxHeight: '100px' }}
        navbarScroll
      >
      <Nav.Link as={Link} to="/">Home</Nav.Link>

      
        <Nav.Link as={Link} to="/product">All</Nav.Link>
        <NavDropdown title="Categories" id="navbarScrollingDropdown">
          <NavDropdown.Item href="#action3">Action</NavDropdown.Item>
          <NavDropdown.Item href="#action4">Another action</NavDropdown.Item>
          <NavDropdown.Divider />
          <NavDropdown.Item href="#action5">
            Something else here
          </NavDropdown.Item>
        </NavDropdown>
      </Nav>
      <Form className="d-flex">
        <FormControl
          type="search"
          placeholder="Search"
          className="me-2"
          aria-label="Search"
        />
        <Button variant="outline-success">Search</Button>
      </Form>
    
      
    </Navbar.Collapse>
    </Fragment>
  }
  { (user.id !== null) ? 
    <Fragment>
     <Nav.Item>
    <Nav.Link as={Link} to="/logout">Logout</Nav.Link>
    </Nav.Item>
   {/* <Nav.Item>
    <Nav.Link as={Link} to="/history">Order history</Nav.Link>
    </Nav.Item>*/}
    </Fragment>
    :
    <Fragment>
    
    <Nav.Item>
      <Nav.Link as={Link} to="/register">Register</Nav.Link>
    </Nav.Item>
    <Nav.Item>
      <Nav.Link as={Link} to="/login">Login</Nav.Link>
    </Nav.Item>
    </Fragment>

  }

  </Container>
</Navbar>
)
}
