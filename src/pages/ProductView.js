import { useState, useEffect, useContext } from 'react';
import {Container, Row, Col, Card, Button} from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function ProductView() {

  const { user } = useContext(UserContext);

  const navigate = useNavigate();

  const { productId } = useParams()

  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(0);



  const order = (productId) => {

    fetch("http://cryptic-savannah-49957.herokuapp.com/caps/users/order", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`
      },
      body: JSON.stringify({
        productId: productId
      })

    }) 
    .then(res => res.json())
    .then(data => {


      if (data === true) {
        Swal.fire({
          title: "Successfully Ordered",
          icon: "success",
          text: "You have successfully order for this item."
        })

        navigate("/product")

      } else {
        Swal.fire({
          title: "Something went wrong",
          icon: "error",
          text: "Please try again"
        })
      }
    })
  }

  useEffect(()=> {
    fetch(`http://cryptic-savannah-49957.herokuapp.com/caps/product/${productId}`)
    .then(res => res.json())
    .then(data => {
      

      setName(data.name)
      setDescription(data.description)
      setPrice(data.price)
    })
  }, [productId])

  return( 
    <Container className='mt-5'>
      <Row>
        <Col lg={{span:6, offset:3}}>
          <Card>
            <Card.Body className="text-center">
              <Card.Title> {name} </Card.Title>
              <Card.Subtitle>Description</Card.Subtitle>
              <Card.Text> {description} </Card.Text>
              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text> {price} </Card.Text>
              {
                user.id !== null?
                  <Button variant="primary" onClick= {() => order(productId)}>Order</Button>
                  :
                  <Link className="btn btn-danger" to="/login">Log in to order</Link>
              }
              
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>

    )

}