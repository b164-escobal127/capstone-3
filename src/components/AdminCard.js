import {useState} from 'react';
import { Row, Col, Card, Button } from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function AdminCard({adminProp}) {

	const {name, description, price, _id} = adminProp


	return(

		<div className='mb-5 m-5'>
<Card.Link as={Link} to={`/create`}>Add a Product</Card.Link>
	<Card>
	<Card.Img variant="top" src="box.jpeg" />
  <Card.Body>
    <Card.Title>{name}</Card.Title>
    <Card.Subtitle className="mb-2 text-muted">Description</Card.Subtitle>
    <Card.Text>
     {description}
    </Card.Text>
     <Card.Subtitle className="mb-2 text-muted">Price:</Card.Subtitle>
    <Card.Text>
     {price}
    </Card.Text>
    <Card.Link as={Link} to={`/update/${_id}`}>Update</Card.Link>
    <Card.Link as={Link} to={`/archive/${_id}`}>Deactivate</Card.Link>
  </Card.Body>
</Card>
	</div>
		)
}