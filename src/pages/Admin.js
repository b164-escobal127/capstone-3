import { Fragment, useEffect, useState } from 'react'
import AdminCard from '../components/AdminCard';

export default function Products() {
	const [product, setProduct] = useState([])

	useEffect(() => {

		fetch("http://cryptic-savannah-49957.herokuapp.com/caps/product/adminProduct")
		.then(res => res.json())
		.then(data => {
			 

			setProduct(data.map(product => {
				return(<AdminCard key={product}adminProp={product} />
					)
			}))
		})
	}, [])


	return(
		<Fragment className= "mb-5">
			<h1>All Products</h1>
			{product}
		</Fragment>

		)

}
