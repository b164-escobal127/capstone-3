import React from "react";

export default function Footer() {
	return (
	
	  <div className="footer">
	    <p>For Educational Purposes only!  |  &copy;2022 Kjames App  |  by Kevin James</p>
	  </div>
	);
}
