import {useState} from 'react';
import { Row, Col, Card, Button } from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function ProductCard({productProp}) {

	const {name, description, price, _id} = productProp

	return(

		<div className='mb-5 m-5'>

	<Card>
	<Card.Img variant="top" src="box.jpeg" />
  <Card.Body>
    <Card.Title>{name}</Card.Title>
    <Card.Subtitle className="mb-2 text-muted">Description</Card.Subtitle>
    <Card.Text>
     {description}
    </Card.Text>
     <Card.Subtitle className="mb-2 text-muted">Price:</Card.Subtitle>
    <Card.Text>
     {price}
    </Card.Text>
    <Card.Link as={Link} to={`/product/${_id}`}>Order</Card.Link>
  </Card.Body>
</Card>
	</div>
		)

}