import { useState, useEffect, useContext } from 'react';
import {Container, Row, Col, Card, Button} from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function ProductView() {

  const { user } = useContext(UserContext);

  const navigate = useNavigate();

  const { productId } = useParams()

  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(0);


const deactivate = (productId) => {

    fetch(`http://cryptic-savannah-49957.herokuapp.com/caps/product/${productId}/archive`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`
      },
      body: JSON.stringify({
        productId: productId
      })

    }) 
    .then(res => res.json())
    .then(data => {

      if (data === true) {
        Swal.fire({
          title: "Successfully Deactivate",
          icon: "success",
          text: "You have successfully deactivated this item."
        })

        navigate("/all")

      } else {
        Swal.fire({
          title: "Something went wrong",
          icon: "error",
          text: "Please try again"
        })
      }
    })
  }

  useEffect(()=> {
    fetch(`http://cryptic-savannah-49957.herokuapp.com/caps/product/${productId}`)
    .then(res => res.json())
    .then(data => {

      setName(data.name)
      setDescription(data.description)
      setPrice(data.price)
    })
  }, [productId])

  return( 
    <Container className='mt-5'>
      <Row>
        <Col lg={{span:6, offset:3}}>
          <Card>
            <Card.Body className="text-center">
              <Card.Title> {name} </Card.Title>
              <Card.Subtitle>Description</Card.Subtitle>
              <Card.Text> {description} </Card.Text>
              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text> {price} </Card.Text>
            
                  <Button variant="primary" onClick= {() => deactivate(productId)}>deactivate</Button>
                 
              
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>

    )

}