import {Row, Col, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function Banner() {

	return(

		<Row>
			<Col className="p-5">
				<h1>Kjames' Shopping Center Market</h1>
				<p>Opportunities to check or get our available merchandise, EVERYWHERE!</p>
				<Button variant="warning" as={Link} to="/product">Shop Now!</Button>
			</Col>
		</Row>

		)

}