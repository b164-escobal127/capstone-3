import { useState, useEffect, useContext } from 'react';
import { Form, Button} from 'react-bootstrap';
import UserContext from '../UserContext';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Create() {

const {user, setUser} = useContext(UserContext);
const navigate = useNavigate()

const [name, setName] = useState('');
const [description, setDescription] = useState('');
const [price, setPrice] = useState('');

const [isActive, setIsActive] = useState(false);


function createProduct(e) {
		e.preventDefault();
		fetch('http://cryptic-savannah-49957.herokuapp.com/caps/product/addProduct', {
			method: "POST",
			headers: {
			    'Content-Type': 'application/json',
			    'Accept': 'application/json',
				Authorization:`Bearer ${ localStorage.getItem( 'token' ) }`
			},
			body: JSON.stringify({
			    name: name,
			    description: description,
			    price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			

			if (data === true) {

			    setName('');
			    setDescription('');
			    setPrice('');

			    Swal.fire({
			        title: 'Create Product Successful',
			        icon: 'success',
			        text: 'Added!'
			    });

			    	navigate("/all")

			} else {

			    Swal.fire({
			        title: 'Something went wrong',
			        icon: 'error',
			        text: 'Please enter a new product again.'   
			    });

			}
		})
	}

	useEffect(() => {
		if(name !== '' && description !== '' && price !== Number ){
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}
	}, [name, description, price])

	return(


	<Form onSubmit= {(e) => createProduct(e)}>
		<h1 className="text-center">Create Product</h1>

		<Form.Group
			className="mb-3"
			controlId="name">
		<Form.Label>Name</Form.Label>
		<Form.Control 
			type="text" 
		    	placeholder="Enter Name"
		    	value={name}
		    	onChange={e => {
		    		setName(e.target.value)
		    		
		    	}} 
		    	required
		/>
		<Form.Text className="text-muted">Please enter name </Form.Text>
		</Form.Group>

		<Form.Group
			className="mb-3"
			controlId="name">
		<Form.Label>Description</Form.Label>
		<Form.Control 
			type="text" 
		    	placeholder="Enter Description"
		    	value={description}
		    	onChange={e => {
		    		setDescription(e.target.value)
		    		
		    	}} 
		    	required
		/>
		<Form.Text className="text-muted">Please enter description </Form.Text>
		</Form.Group>

		<Form.Group
			className="mb-3"
			controlId="name">
		<Form.Label>Price</Form.Label>
		<Form.Control 
			type="number" 
		    	placeholder="Enter Price"
		    	value={price}
		    	onChange={e => {
		    		setPrice(e.target.value)
		    		
		    	}} 
		    	required
		/>
		<Form.Text className="text-muted">Please enter price </Form.Text>
		</Form.Group>

		{
		  	isActive ?
		  	<Button variant="primary" type="submit" id="submitBtn">
		  	  Submit
		  	</Button>
		  	:
		  	<Button variant="danger" type="submit" id="submitBtn" disabled>
		  	  Submit
		  	</Button>

		  }

		  </Form>






		)

}
