import { Fragment, useEffect, useState } from 'react'
import ProductCard from '../components/ProductCard';

export default function Products() {
	const [product, setProduct] = useState([])

	useEffect(() => {

		fetch("http://cryptic-savannah-49957.herokuapp.com/caps/product/allActive")
		.then(res => res.json())
		.then(data => {
			 

			setProduct(data.map(product => {
				return(<ProductCard key={product._id}productProp={product} />
					)
			}))
		})
	}, [])


	return(
		<Fragment className= "mb-5">
			<h1>Products</h1>
			{product}
		</Fragment>

		)

}
